// Common Exmples of Arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Leo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
let room = ["401A", "401B", "401C"];

// Not recommended use of array
let mixedArray = [12, "Asus", null, undefined, {}];

// Alternative awy to write arrays
let myTasks = [
	"drink HTML",
	"eat JavaScript",
	"inhale CSS",
	"bake SASS"
];
// In locating Array always start at 0
// index = n - 1 where n is the total length of array

// Reassigning array values
console.log("Array before reassignment");
console.log(myTasks);
myTasks[0] = "clean Node"
console.log("Array after reassignment");
console.log(myTasks);

// Reading from Array
console.log(grades[0]);
console.log(computerBrands[3]);

// Accessing an array element that doesn't exist
console.log(grades[20]); 

// Getting the length of an array (.length)
console.log(computerBrands.length);

// Manipulating Arrays
// Array Methods

/*Mutator Methods
	-functions that "mutate" or change an array after they're created.
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

/*push()
	-adds an element in the end of an array and returns the array's length.
*/
console.log("Current Array:");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log(fruits);

/*pop()
	-removes the last element in an array and returns the removed element
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);

/*unshift()
	-add 1 or more elements at the beginning of an array
	
	Syntax:
	arrayName.unshift("elementA");
	arrayName.unshift("elementA", "elementB");
*/
fruits.unshift("Lime", "banana")
console.log("Mutated array from unshift method:");
console.log(fruits);

/*shift()
	-removes an element at the beginning of an array and returns the removed element

	Syntax:
	arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

/*splice()
	-simultaneously removes elements from a specified index number and adds elements.

	Syntax:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits);

/*sort()
	-rearranges the array elements in alphanumeric order.

	Syntax:
	arrayName.sort()
*/
fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

/*reverse()
	-reverses the order of array elements.

	Syntax:
	arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);

/*Non-mutator methods
	-functions that do not modify or change an array after they're created.
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

/*indexOf()
	-returns the index number of the first matching element found in an array.

	Syntax:
	arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

/*slice()
	-portions/slices elements from an array and returns a new array.

	Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);
*/
console.log(countries);
// Slicing elements from a specified index to another index
let slicedArrayA = countries.slice(2);
console.log("Result form slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 5);
console.log("Result from slice method:");
console.log(slicedArrayB);

/*toString()
	- returns an array as a string separated by commas.

	Syntax:
	arrayName.toString();
*/
let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);

/*concat()
	-combines two arrays and returns the combined result

	Syntax:
	arrayA.concat(arrayB);
*/
let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

// combining multiple arrays
console.log("Result from concat method:");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log(combinedTasks);

/*join()
	-returns an array as a string separated by a specified separator string.

	Syntax:
	arrayName.join("separatorString");
*/
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));

/* Iteration Methods
	-are loops designed to perform repetitive tasks on arrays.
*/
// Common practice to use singular form of the array content for paramaeter names used in array loops.

/*forEach()
	-similar to a for loop that iterates on each array element.

	Syntax:
	arrayName.forEach(function(indivElement){
		statement;
	})
*/
console.log("Result from forEach method:");
allTasks.forEach(function(task){
	console.log(task);
});

// Using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task)
	}
});
console.log("Result of filtered tasks:")
console.log(filteredTasks);

/*map()
	-it iterates on each element and returns a new array with different values depending on the result of the function's operation.

	Syntax:
	let/const resultArray = arrayName.map(function(indivElement){
		statement/s;
	});
*/
let numbers = [1, 2, 3, 4, 5]
let numberMap = numbers.map(function(number){
	return number * number;
});
console.log("Result of map method:")
console.log(numberMap);

/*every()
	-checks if all elements in an array meet the given condition.

	Syntax:
	let/const resultArray = arrayName.every(function(indivElement){
		return expression/condition;
	});
*/
let allValid = numbers.every(function(number){
	return (number < 3);
});
console.log("Result of every method:")
console.log(allValid);

/*some()
	-this will check at least element in the array meets the given condition.

	Syntax:
	let/const resultArray = arrayName.some(function(indivElement){
		return expression/condition;
	});
*/
let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result of some method:")
console.log(someValid);